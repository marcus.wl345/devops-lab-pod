# DevOps-Lab-Pod

This instructions are really simple.  The source in this repo produces an broken docker container (or pod in K8) can you fix it?

Please submit your solution with the following 
* Screen Shots of the working problem 
* Any changes to you made to get it to work

It should take no more that 30 minutes to fix. 

## Thought Process.
1. Check if Go code worked (No problems)
2. Check if `docker build` worked (Issue on layer 5). Problem can be fixed by doing 2 actions.
    1. Initialise a Go Module. (Preferred solution as it gave the code base a proper dependency management system for future)
    2. Change command to `go build -o app main.go` (More of a hack)
3. Check if docker image works. (Timezone package issue. A quick Google search indicated that the `time go library` required files from the base OS. Just installed the `tzdata` apk. Would have rather a solution that used native Go libraries instead of a random apk. How are the devs supposed to know??)

## Screenshot
![alt text](./assets/Screenshot.png "Screenshot of Solution")
